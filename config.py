import os
from secret_keys import SecretKeys


key = SecretKeys
POSTGRES_PW=key.POSTGRES_PW
POSTGRES_URL=key.POSTGRES_URL
POSTGRES_DB=key.POSTGRES_DB
POSTGRES_USER=key.POSTGRES_USER


class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY") or "you-will-never-guess"
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DB_URL = "postgresql+psycopg2://{user}:{pw}@{url}/{db}".format(
        user=POSTGRES_USER, pw=POSTGRES_PW, url=POSTGRES_URL, db=POSTGRES_DB
    )
    SQLALCHEMY_DATABASE_URI = DB_URL
    COURSES_PER_PAGE = 4
