from app_package import app, db
from app_package.models import (
    User,
    Courses,
    Chapters,
    Videos,
    subscriptions_table,
    completion_table,
)


@app.shell_context_processor
def make_shell_context():
    return {
        "db": db,
        "User": User,
        "Courses": Courses,
        "Chapters": Chapters,
        "Videos": Videos,
        "subscriptions_table": subscriptions_table,
        "completion_table": completion_table,
    }


if __name__ == "__main__":
    app.run()
