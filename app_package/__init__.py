from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager, login_required
import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import os
from flask_bootstrap import Bootstrap


app = Flask(__name__)
app.config.from_object(Config)
login = LoginManager(app)
login.login_view = 'authentication.login'
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bootstrap = Bootstrap(app)


from app_package.errors import bp as errors_bp
app.register_blueprint(errors_bp)

from app_package.authentication import bp as authentication_bp
app.register_blueprint(authentication_bp, url_prefix='/authentication')

from app_package import routes, models