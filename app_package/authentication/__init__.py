from flask import Blueprint

bp = Blueprint("authentication", __name__)

from app_package.authentication import forms, routes
