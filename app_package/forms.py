from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, PasswordField, BooleanField, SubmitField, RadioField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length
from app_package.models import *





class CreateCourseForm(FlaskForm):
    course_name = StringField('Course Name', validators=[DataRequired(), Length(min=0, max=50)])
    about_course = TextAreaField('About Course', validators=[DataRequired(), Length(min=0, max=150)])
    submit = SubmitField('Create Course')


class AddChapterForm(FlaskForm):
    chapter_name = StringField('Chapter Name', validators=[DataRequired(), Length(min=0, max=50)])
    submit = SubmitField('Add Chapter')

class AddVideoForm(FlaskForm):
    description = TextAreaField('Video Description', validators=[DataRequired(), Length(min=0, max=150)])
    link = StringField('Paste the youtube embed video link/url', validators=[DataRequired(), Length(min=0, max=300)])
    submit = SubmitField('Add Video')

class MarkCompletedForm(FlaskForm):
    mark = BooleanField('Mark As Completed')
    submit = SubmitField('Add Video')
