from werkzeug.security import generate_password_hash, check_password_hash
from app_package import db
from app_package import login
from flask_login import UserMixin
from hashlib import md5


subscriptions_table = db.Table(
    "subscriptions_table",
    db.Column("user_id", db.Integer, db.ForeignKey("user.id")),
    db.Column("course_id", db.Integer, db.ForeignKey("courses.id")),
    db.PrimaryKeyConstraint("user_id", "course_id"),
)

completion_table = db.Table(
    "completion_table",
    db.Column("user_id", db.Integer, db.ForeignKey("user.id")),
    db.Column("video_id", db.Integer, db.ForeignKey("videos.id")),
    db.PrimaryKeyConstraint("user_id", "video_id"),
)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    role = db.Column(db.String(64))
    subscribed = db.relationship(
        "Courses",
        secondary=subscriptions_table,
        backref=db.backref("subscribed_by", lazy="dynamic"),
        lazy="dynamic",
    )
    completed_watching = db.relationship(
        "Videos",
        secondary=completion_table,
        backref=db.backref("watched_by", lazy="dynamic"),
        lazy="dynamic",
    )

    def __repr__(self):
        return "<User {}>".format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode("utf-8")).hexdigest()
        return "https://www.gravatar.com/avatar/{}?d=identicon&s={}".format(
            digest, size
        )

    def subscribed_courses(self):
        subscribed_course = Courses.query.join(
            subscriptions_table, (subscriptions_table.c.course_id == Courses.id)
        ).filter(subscriptions_table.c.user_id == self.id)
        return subscribed_course


class Courses(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_name = db.Column(db.String(128), index=True)
    author_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    about_course = db.Column(db.String(150))

    def __repr__(self):
        return "<Course {}>".format(self.about_course)


class Chapters(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chapter_name = db.Column(db.String(128), index=True)
    course_id = db.Column(db.Integer, db.ForeignKey("courses.id"))
    chapter_number = db.Column(db.Integer)

    def __repr__(self):
        return "<Chapter : {} Chapter {}>".format(self.course_id, self.chapter_name)

    def update_position(self):
        position = Chapters.query.filter_by(course_id=self.course_id).all()
        count = len(position)
        self.chapter_number = count + 1


class Videos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(128), index=True)
    link = db.Column(db.String(300))
    chapter_id = db.Column(db.Integer, db.ForeignKey("chapters.id"))
    video_number = db.Column(db.Integer)

    def __repr__(self):
        return "<description : {} >".format(self.description)

    def update_position(self):
        position = Videos.query.filter_by(chapter_id=self.chapter_id).all()
        count = len(position)
        self.video_number = count + 1
