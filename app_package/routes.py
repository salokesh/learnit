from flask import request, render_template, flash, redirect, url_for, abort
from app_package import app, db
from flask_login import current_user, login_required
from app_package.models import Courses, Chapters, Videos
from app_package.forms import (
    CreateCourseForm,
    AddChapterForm,
    AddVideoForm,
    MarkCompletedForm,
)


@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if not current_user.is_authenticated:
        return abort(401)
    page = request.args.get("page", 1, type=int)
    courses = current_user.subscribed_courses().paginate(
        page, app.config["COURSES_PER_PAGE"], False
    )
    next_url = url_for("index", page=courses.next_num) if courses.has_next else None
    prev_url = url_for("index", page=courses.prev_num) if courses.has_prev else None
    return render_template(
        "index.html",
        title="Home",
        courses=courses.items,
        next_url=next_url,
        prev_url=prev_url,
    )


@app.route("/explore")
@login_required
def explore():
    page = request.args.get("page", 1, type=int)
    courses = Courses.query.order_by(Courses.id).paginate(
        page, app.config["COURSES_PER_PAGE"], False
    )
    next_url = url_for("explore", page=courses.next_num) if courses.has_next else None
    prev_url = url_for("explore", page=courses.prev_num) if courses.has_prev else None
    return render_template(
        "explore.html",
        title="explore",
        courses=courses.items,
        user=current_user,
        next_url=next_url,
        prev_url=prev_url,
    )


@app.route("/user/<username>/create_course", methods=["GET", "POST"])
@login_required
def create_course(username):
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    form = CreateCourseForm()
    if current_user.username != username:
        return abort(403)
    if not current_user.is_authenticated:
        return abort(401)
    if current_user.role == "Trainee":
        return abort(403)
    if form.validate_on_submit():
        course = Courses(
            course_name=form.course_name.data,
            about_course=form.about_course.data,
            author_id=current_user.id,
        )
        db.session.add(course)
        db.session.commit()
        flash("Course Created successfully!!!")
        return redirect(
            url_for(
                "add_chapter",
                username=current_user.username,
                course_name=form.course_name.data,
            )
        )
    return render_template("add_courses.html", title="Add Course", form=form)


@app.route("/user/<username>/my_courses", methods=["GET", "POST"])
@login_required
def my_courses(username):
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if current_user.username != username:
        return abort(403)
    if not current_user.is_authenticated:
        return abort(401)
    if current_user.role == "Trainee":
        return abort(403)
    courses = Courses.query.filter_by(author_id=current_user.id).all()
    return render_template("my_courses.html", title="My Courses", courses=courses)


@app.route(
    "/user/<username>/courses/<course_name>/add_chapter", methods=["GET", "POST"]
)
@login_required
def add_chapter(username, course_name):
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if username != current_user.username:
        return abort(403)
    if not current_user.is_authenticated:
        return abort(401)
    if current_user.role == "Trainee":
        return abort(403)
    print(current_user.role)
    form = AddChapterForm()
    course = Courses.query.filter_by(
        author_id=current_user.id, course_name=course_name
    ).first_or_404()
    chapter_list = Chapters.query.filter_by(course_id=course.id).all()
    if form.validate_on_submit():
        chapter = Chapters(chapter_name=form.chapter_name.data, course_id=course.id)
        chapter.update_position()
        db.session.add(chapter)
        db.session.commit()
        flash("Chapter added successfully!!!")
        return redirect(
            url_for(
                "add_chapter",
                username=current_user.username,
                course_name=course.course_name,
            )
        )
    return render_template(
        "add_chapter.html",
        title="My Courses",
        course=course,
        form=form,
        chapters=chapter_list,
    )


@app.route(
    "/user/Courses/<course_id>/chapters/<chapter_id>/add_video/",
    methods=["GET", "POST"],
)
@login_required
def add_video(course_id, chapter_id):

    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if not current_user.is_authenticated:
        return abort(401)
    if current_user.role == "Trainee":
        return abort(403)
    form = AddVideoForm()
    course = Courses.query.filter_by(id=course_id).first_or_404()
    chapter = Chapters.query.filter_by(id=chapter_id).first_or_404()
    videos = Videos.query.filter_by(chapter_id=chapter_id)
    if form.validate_on_submit():
        video = Videos(
            description=form.description.data,
            link=form.link.data,
            chapter_id=chapter_id,
        )
        video.update_position()
        db.session.add(video)
        db.session.commit()
        flash("Video added successfully!!!")
        return redirect(
            url_for("add_video", course_id=course_id, chapter_id=chapter_id)
        )
    return render_template(
        "add_video.html",
        title="Add videos",
        course=course,
        chapter=chapter,
        form=form,
        videos=videos,
    )


@app.route("/user/courses/<course_id>/", methods=["GET", "POST"])
@login_required
def course_details(course_id):
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if not current_user.is_authenticated:
        return abort(401)
    all_videos = db.session.execute(
        f"""select videos.description  from courses join chapters on courses.id=chapters.course_id join videos on chapters.id=videos.chapter_id where chapters.course_id={course_id}"""
    )
    video_list = [video for video in all_videos]
    total_videos = len(video_list)
    if len(video_list) > 0:
        watched_videos = db.session.execute(
            f"""select * from completion_table as c join Videos as v on c.video_id=v.id join Chapters as c1 on v.chapter_id=c1.id join Courses as c2 on c1.course_id=c2.id where c.user_id={current_user.id} and c2.id={course_id}"""
        )
        watched_videos_list = [videos for videos in watched_videos]
        watched_videos_count = len(watched_videos_list)
        percentage = int((watched_videos_count / total_videos) * 100)
    else:
        percentage = 0
    course = Courses.query.filter_by(id=course_id).first_or_404()
    chapters = Chapters.query.filter_by(course_id=course_id).all()
    return render_template(
        "course_details.html", course=course, chapters=chapters, percentage=percentage
    )


@app.route("/user/Courses/<course_id>/chapters/<chapter_id>", methods=["GET", "POST"])
@login_required
def chapter_details(course_id, chapter_id):
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if not current_user.is_authenticated:
        return abort(401)

    chapter = Chapters.query.filter_by(id=chapter_id).first_or_404()
    course = Courses.query.filter_by(id=chapter.course_id).first_or_404()
    videos = Videos.query.filter_by(chapter_id=chapter_id).all()
    return render_template(
        "chapter_details.html", chapter=chapter, videos=videos, course=course
    )


@app.route("/user/Courses/<course_id>/chapters/<chapter_id>/videos/<video_id>/watch", methods=["GET", "POST"])
@login_required
def watch(course_id, chapter_id, video_id):
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if not current_user.is_authenticated:
        return abort(401)
    form = MarkCompletedForm()
    video = Videos.query.filter_by(id=video_id).first_or_404()
    chapter = Chapters.query.filter_by(id=video.chapter_id).first_or_404()
    course = Courses.query.filter_by(id=chapter.course_id).first_or_404()
    link = video.link
    if form.validate_on_submit():
        completed = form.mark.data
        if completed:
            video = Videos.query.filter_by(id=video_id).first_or_404()
            if video not in current_user.completed_watching.all():
                current_user.completed_watching.append(video)
                db.session.commit()
        return redirect(
            url_for("chapter_details", course_id=course.id, chapter_id=video.chapter_id)
        )
    return render_template(
        "play_video.html",
        link=link,
        video=video,
        form=form,
        chapter=chapter,
        course=course,
    )


@app.route("/subscribe/<course_id>/", methods=["GET", "POST"])
@login_required
def subscribe(course_id):
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if not current_user.is_authenticated:
        return abort(401)
    course = Courses.query.filter_by(id=course_id).first_or_404()
    current_user.subscribed.append(course)
    db.session.commit()
    return redirect(url_for("course_details", course_id=course_id))


@app.route("/unsubscribe/<course_id>/", methods=["GET", "POST"])
@login_required
def unsubscribe(course_id):
    if current_user.is_anonymous:
        return redirect(url_for("authentication.login"))
    if not current_user.is_authenticated:
        return abort(401)
    course = Courses.query.filter_by(id=course_id).first_or_404()
    if current_user not in course.subscribed_by:
        return abort(403)
    course = Courses.query.filter_by(id=course_id).first_or_404()
    current_user.subscribed.remove(course)
    db.session.commit()
    return redirect(url_for("index"))
